
- Jenkins had to create the following file once
  ```
  vi /var/lib/jenkins/workspace/InAppBI-Dev-Bundled-Future/inappbi/biadmin/.tmp/scripts/configs/appconfig.js
  chmod 777  /var/lib/jenkins/workspace/InAppBI-Dev-Bundled-Future/inappbi/biadmin/.tmp/scripts/configs/appconfig.js
  ```

- Running AWS Configure so that Jenkins can push the code

  ```
  sudo su -s /bin/bash jenkins
  aws configure
  ```